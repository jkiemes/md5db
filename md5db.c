#include <assert.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <errno.h>

#define u8  unsigned char
#define u32 uint32_t
#include "jhash.h"

#define MAXSHIFT 16
#define HEADERLEN 4096
#define MAX_LINESIZE 1000
#define WINDOW 20
#define MAXFILENAMELEN 4000

#define NR_LOCATION 8

const char md5_database_default[] = "/home/jochen/src/3TB.md5.data/all/db/md5.db";
const char fn_database_default[]  = "/home/jochen/src/3TB.md5.data/all/db/fn.db";

//----------------------------------------------------
// MD5 Database 
#define MD5_DATABASE_MAGIC "# MD5-DB"
char *md5_header;
struct md5_ent_s {
	char md5[32];
	char spc1;
	char location[NR_LOCATION];
	char spc2;
	char qual;
	char filelen[10];
	char spc3;
	char namehash[8];
	char nl;
} *md5_addr;
int md5_bits;
long md5_addr_n;
int md5_fd;

//----------------------------------------------------
// FN Database 
#define FN_DATABASE_MAGIC "# FN5-DB"
char *fn_header;
struct fn_ent_s {
	char hash[8];
	char index;
	char spc1;
	char dirhash[8];
	char spc2;
	char namelen[2];
	char spc3;
	char name[32];
	char nl;
} *fn_addr;
int fn_bits;
long fn_addr_n;
int fn_fd;

//----------------------------------------------------
// Command line options
struct opt_s {
	int verbose;
	int ignore;
	char add_code;
	char del_code;
	int filter_loc_count;
	char filter_location[128];
	int readsize;
	int filter_type;
	int private;
	int public;
	int calc_stats;
	const char *md5_database;
	const char *fn_database;
	char *dirprefix;
	int enable_write;
	int len_1kblks;
	char *format;
} cmdopt = {
	/* verbose         */ 0,
	/* ignore          */ 0,
	/* add_code        */ ' ',
	/* del_code        */ ' ',
	/* filter_loc_count*/ -1,
        /* filter_location */ {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                               0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                               0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                               0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                               0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                               0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                               0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                               0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
	/* readsize        */ 0,
	/* filter_type     */ 0,
	/* private         */ 0,
	/* public          */ 0,
	/* calc_stats      */ 0,
	/*  *md5_database  */ NULL,
	/*  *fn_database   */ NULL,
	/*  *dirprefix     */ NULL,
	/* enable_write    */ 0,
	/* len_1kblks      */ 0,
	/* format          */ "%l\n"
};

struct stats_s {
	long known;
	long added;
	long filesadded;
	long lengthset;
	long dirsadded;
	
	long files_without_length;
	long long sum_all_entries;
	long long sum_all_ignored;
	long long sum_all_without_location;
	long long sum_all_ok;
	long long sum_location[256];
	long long sum_obs_location[256];
	long long sum_unq_location[256];
	long      sum_len_location[256];
	long      sum_unn_location[256];
} dbstat = { 0,0,0,0,0,0,0,0,0,0 };

char location[256][80];

const struct md5_ent_s md5_empty = {
        {'#','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.',
         '.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.'},
	' ',
        {'_','_','_','_','_','_','_','_'},
	' ',
	'.',
        {'.','.','.','.','.','.','.','.','.','.'},
	' ',
        {'.','.','.','.','.','.','.','.'},
	'\n'
};

const struct fn_ent_s fn_empty = {
        {'#','.','.','.','.','.','.','.'},
	'.',
	' ',
        {'.','.','.','.','.','.','.','.'},
	' ',
        {'.','.'},
	' ',
        {'_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_',
	 '_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_'},
	'\n'
};

const int mask[256] = { 
		['0'] = 0,
		['1'] = 1,
		['2'] = 2,
		['3'] = 3,
		['4'] = 4,
		['5'] = 5,
		['6'] = 6,
		['7'] = 7,
		['8'] = 8,
		['9'] = 9,
		['a'] = 10,['A'] = 10,
		['b'] = 11,['B'] = 11,
		['c'] = 12,['C'] = 12,
		['d'] = 13,['D'] = 13,
		['e'] = 14,['E'] = 14,
		['f'] = 15,['F'] = 15
	};

u32 calc_namehash(const char *name) {
	return jhash(name,strlen(name),0xdeadbeaf);
}

long get_index(const char *hex,int bits) {
	long p;
	int i;
	p=0;
	for (i = 0;i <= bits;i += 4) {
		p <<= 4;
		p += mask[(int)*hex++];
	}
	while (i > bits) {
		p >>= 1;
		i--;
	}
	return p;
}

long find_md5(const char *md5) {
	long p,p_u,p_o,p_0;
	struct md5_ent_s *e;
	int i;
	
	//fprintf(stderr,"find_md5: %.32s\n",md5);
	p = get_index(md5,md5_bits);
	if (md5_addr[p].md5[0] == '#')
		return -1;

	// Instead of doing several string compares (smaller/bigger) we check
	// for the last char (inside a window of WINDOW entries, rarely two compares
	// are needed). If it fits, we can compare all.

	p_0 = p;
	p_u = p < WINDOW ? 0 : p-WINDOW;
	p_o = p > md5_addr_n-WINDOW-1 ? md5_addr_n-1 : p+WINDOW;

	for (p = p_0,e = &md5_addr[p_0];p <= p_o;p++,e++) { // this includes p_0
		i = 31;
		while (e->md5[i] == md5[i]) {
			if (i-- == 0)
				return p;
		}
	}
	for (p = p_0-1,e = &md5_addr[p_0-1];p >= p_u;p--,e--) {
		i = 31;
		while (e->md5[i] == md5[i]) {
			if (i-- == 0)
				return p;
		}
	}
	return -1;
}

long find_fn(const char *hash,char index) {
	long p,p_u,p_o,p_0;
	struct fn_ent_s *e;
	int i;
	
	//fprintf(stderr,"find_fn: %.8s\n",hash);
	p = get_index(hash,fn_bits);
	if (fn_addr[p].hash[0] == '#')
		return -1;

	// Instead of doing several string compares (smaller/bigger) we check
	// for the last char (inside a window of WINDOW entries, rarely two compares
	// are needed). If it fits, we can compare all.

	p_0 = p;
	p_u = p < WINDOW ? 0 : p-WINDOW;
	p_o = p > fn_addr_n-WINDOW-1 ? fn_addr_n-1 : p+WINDOW;

	for (p = p_0,e = &fn_addr[p_0];p <= p_o;p++,e++) { // this includes p_0
		if (index == e->index) {
			i = 7;
			while (e->hash[i] == hash[i]) {
				if (i-- == 0)
				      return p;
			}
		}
	}
	for (p = p_0-1,e = &fn_addr[p_0-1];p >= p_u;p--,e--) {
		if (index == e->index) {
			i = 7;
			while (e->hash[i] == hash[i]) {
				if (i-- == 0)
				      return p;
			}
		}
	}
	return -1;
}

long find_free_md5_entry(const char *md5) {
	long p,p_0,p_u,p_o,p_u_free,p_u2_free,p_o_free,p_o2_free,n_u,n_o;
	struct md5_ent_s *e_u,*e_o;

	p_0 = get_index(md5,md5_bits);
	e_u = e_o = &md5_addr[p_0];
	if (e_o->md5[0] == '#')
		return p_0;

	p_u = p_0 < WINDOW ? 0 : p_0-WINDOW;
	p_o = p_0 > md5_addr_n-WINDOW-1 ? md5_addr_n-1 : p_0+WINDOW;

	p_u_free = p_o_free = -1;
	n_u = n_o = 0;
	for (p = p_0+1;p <= p_o;p++) {
 		if ((++e_o)->md5[0] == '#') {
			n_o++;
			if (p_o_free < 0)
				p_o_free = p_o2_free = p;
			else
				p_o2_free = p;
		}
	}
	for (p = p_0-1;p >= p_u;p--) {
 		if ((--e_u)->md5[0] == '#') {
			n_u++;
			if (p_u_free < 0)
				p_u_free = p_u2_free = p;
			else
				p_u2_free = p;
		}
	}
	if (n_o > n_u)
	        return n_u > 0 ? p_o_free : p_o2_free;
	if (n_o < n_u)
	        return n_o > 0 ? p_u_free : p_u2_free;
	if (n_o == 0)
		return -1;
	return (p_o_free+p_u_free >= md5_addr_n) ? p_u_free :  p_o_free;
}

long find_free_fn_entry(const char *hash) {
	long p,p_0,p_u,p_o,p_u_free,p_u2_free,p_o_free,p_o2_free,n_u,n_o;
	struct fn_ent_s *e_u,*e_o;

	p_0 = get_index(hash,fn_bits);
	e_u = e_o = &fn_addr[p_0];
	if (e_o->hash[0] == '#')
		return p_0;

	p_u = p_0 < WINDOW ? 0 : p_0-WINDOW;
	p_o = p_0 > fn_addr_n-WINDOW-1 ? fn_addr_n-1 : p_0+WINDOW;

	p_u_free = p_o_free = -1;
	n_u = n_o = 0;
	for (p = p_0+1;p <= p_o;p++) {
 		if ((++e_o)->hash[0] == '#') {
			n_o++;
			if (p_o_free < 0)
				p_o_free = p_o2_free = p;
			else
				p_o2_free = p;
		}
	}
	for (p = p_0-1;p >= p_u;p--) {
 		if ((--e_u)->hash[0] == '#') {
			n_u++;
			if (p_u_free < 0)
				p_u_free = p_u2_free = p;
			else
				p_u2_free = p;
		}
	}
	if (n_o > n_u)
	        return n_u > 0 ? p_o_free : p_o2_free;
	if (n_o < n_u)
	        return n_o > 0 ? p_u_free : p_u2_free;
	if (n_o == 0)
		return -1;
	return (p_o_free+p_u_free >= fn_addr_n) ? p_u_free : p_o_free;
}

void prepare_md5(int fd) {
	struct stat sb;
	long tmp;
	int map;
	int prot;

	map  = cmdopt.enable_write ? MAP_SHARED : MAP_PRIVATE;
	prot = cmdopt.enable_write ? PROT_WRITE : PROT_READ;

	if (fstat(fd, &sb) == -1) {
		fprintf(stderr,"Cannot fstat database file\n");
		exit(100);
	}

	if (md5_addr)
		munmap(md5_addr,md5_addr_n * sizeof(struct md5_ent_s));
	if (md5_header)
		munmap(md5_header,HEADERLEN);
	md5_addr_n = 0;
	if (sb.st_size > HEADERLEN)
	        md5_addr_n = (sb.st_size-HEADERLEN)/sizeof(struct md5_ent_s);
	if (md5_addr_n > 0) {
		if (md5_addr_n * sizeof(struct md5_ent_s) != sb.st_size-HEADERLEN) {
			fprintf(stderr,"Database len is wrong\n");
			exit(100);
		}

		md5_header = mmap(NULL,HEADERLEN,prot,map,fd,0);
		if (strncmp(md5_header,MD5_DATABASE_MAGIC,8) != 0) {
		        fprintf(stderr,"Missing database magic: %s\n",MD5_DATABASE_MAGIC);
			exit(10);
		}
		md5_addr = mmap(NULL,md5_addr_n * sizeof(struct md5_ent_s),prot,map,fd,HEADERLEN);
		madvise(md5_addr,md5_addr_n * sizeof(struct md5_ent_s),MADV_RANDOM|MADV_UNMERGEABLE);
		if ((md5_header == MAP_FAILED) || (md5_addr == MAP_FAILED)) {
			fprintf(stderr,"MMAP call failed\n");
			exit(100);
		}
		for (md5_bits = 0,tmp = md5_addr_n;(tmp & 1) == 0;tmp >>= 1)
		      md5_bits++;
		if (tmp != 1) {
			fprintf(stderr,"Number of elements is not a multiple of 2\n");
			exit(100);
		}
		//fprintf(stderr,"bits=%d    n=%ld\n",md5_bits,md5_addr_n);
	}
	else {
		md5_header = NULL;
		md5_addr = NULL;
		md5_bits = 0;
	}
}

void prepare_fn(int fd) {
	struct stat sb;
	long tmp;
	int map;
	int prot;

	map  = cmdopt.enable_write ? MAP_SHARED : MAP_PRIVATE;
	prot = cmdopt.enable_write ? PROT_WRITE : PROT_READ;

	if (fstat(fd, &sb) == -1) {
		fprintf(stderr,"Cannot fstat database file\n");
		exit(100);
	}

	if (fn_addr)
		munmap(fn_addr,fn_addr_n * sizeof(struct fn_ent_s));
	if (fn_header)
		munmap(fn_header,HEADERLEN);
	fn_addr_n = 0;
	if (sb.st_size > HEADERLEN)
	        fn_addr_n = (sb.st_size-HEADERLEN)/sizeof(struct fn_ent_s);
	if (fn_addr_n > 0) {
		if (fn_addr_n * sizeof(struct fn_ent_s) != sb.st_size-HEADERLEN) {
			fprintf(stderr,"Database len is wrong\n");
			exit(100);
		}

		fn_header = mmap(NULL,HEADERLEN,prot,map,fd,0);
		if (strncmp(fn_header,FN_DATABASE_MAGIC,8) != 0) {
		        fprintf(stderr,"Missing database magic: %s\n",FN_DATABASE_MAGIC);
			exit(10);
		}
		fn_addr = mmap(NULL,fn_addr_n * sizeof(struct fn_ent_s),prot,map,fd,HEADERLEN);
		madvise(fn_addr,fn_addr_n * sizeof(struct fn_ent_s),MADV_RANDOM|MADV_UNMERGEABLE);
		if ((fn_header == MAP_FAILED) || (fn_addr == MAP_FAILED)) {
			fprintf(stderr,"MMAP call failed\n");
			exit(100);
		}
		for (fn_bits = 0,tmp = fn_addr_n;(tmp & 1) == 0;tmp >>= 1)
		      fn_bits++;
		if (tmp != 1) {
			fprintf(stderr,"Number of elements is not a multiple of 2\n");
			exit(100);
		}
		//fprintf(stderr,"bits=%d    n=%ld\n",fn_bits,fn_addr_n);
	}
	else {
		fn_header = NULL;
		fn_addr = NULL;
		fn_bits = 0;
	}
}

void grow_md5(int fd, long goal) {
	long old,p,x;
	struct md5_ent_s tmp[128],*e,*t;

	if (!cmdopt.enable_write) {
		fprintf(stderr,"Write not enabled with -w\n");
		exit(100);
	}
	old = md5_addr_n;
	posix_fallocate(fd,0,HEADERLEN+goal*sizeof(struct md5_ent_s));
	prepare_md5(fd); // this updates n
	fprintf(stderr,"grow md5...goal=%ld",goal);
	for (p = old,e = &md5_addr[old];p < md5_addr_n;p++)  // Initialize new items to empty
		*e++ = md5_empty;
		
	for (p = (old < 128 ? old:128),e = &md5_addr[0],t = &tmp[0];p > 0;p--) { // Save first 128 entries
		*t++ = *e;
		*e++ = md5_empty;
	}
	
	for (p = old,e = &md5_addr[old];p > 0;p--) {
		e--;
		if (e->md5[0] != '#') {
			x = find_free_md5_entry(e->md5);
			assert(p < x);
			md5_addr[x] = *e;
			*e = md5_empty;
		}
	}
	
	for (p = (old < 128 ? old:128),t = &tmp[0];p > 0;p--,t++) { // Restore first entries
		if (t->md5[0] != '#') {
			x = find_free_md5_entry(t->md5);
			assert(x >= 0);
			md5_addr[x] = *t;
		}
	}
	fprintf(stderr,"...done\n");
}

void grow_fn(int fd, long goal) {
	long old,p,x;
	struct fn_ent_s tmp[128],*e,*t;

	if (!cmdopt.enable_write) {
		fprintf(stderr,"Write not enabled with -w\n");
		exit(100);
	}
	old = fn_addr_n;
	posix_fallocate(fd,0,HEADERLEN+goal*sizeof(struct fn_ent_s));
	prepare_fn(fd); // this updates n
	fprintf(stderr,"grow fn...goal=%ld",goal);

	for (p = old,e = &fn_addr[old];p < fn_addr_n;p++)  // Initialize new items to empty
		*e++ = fn_empty;
		
	for (p = (old < 128 ? old:128),e = &fn_addr[0],t = &tmp[0];p > 0;p--) { // Save first 128 entries
		*t++ = *e;
		*e++ = fn_empty;
	}
	
	for (p = old,e = &fn_addr[old];p > 0;p--) {
		e--;
		if (e->hash[0] != '#') {
			x = find_free_fn_entry(e->hash);
			assert(p < x);
			fn_addr[x] = *e;
			*e = fn_empty;
		}
	}
	
	for (p = (old < 128 ? old:128),t = &tmp[0];p > 0;p--,t++) { // Restore first entries
		if (t->hash[0] != '#') {
			x = find_free_fn_entry(t->hash);
			assert(x >= 0);
			fn_addr[x] = *t;
		}
	}
	fprintf(stderr,"...done\n");
}

#define FILTER_PASS_UNKNOWN       1
#define FILTER_PASS_KNOWN         (2+1024)
#define FILTER_PASS_IGNORE        (2+4)
#define FILTER_REMOVE_IGNORE      (2+8)
#define FILTER_PASS_NAMED         (2+16)
#define FILTER_REMOVE_NAMED       (2+32)
#define FILTER_PASS_WITHSIZE      (2+64)
#define FILTER_REMOVE_WITHSIZE    (2+128)
#define FILTER_PASS_PRIVATE       (2+256)
#define FILTER_REMOVE_PRIVATE     (2+512)
#define FILTER_LOCATION	          (2+2048)
#define FILTER_ENABLE		  8192

#define FILTER_KNOWN    (FILTER_PASS_UNKNOWN  | FILTER_PASS_KNOWN)
#define FILTER_IGNORE   (FILTER_PASS_IGNORE   | FILTER_REMOVE_IGNORE)
#define FILTER_NAMED    (FILTER_PASS_NAMED    | FILTER_REMOVE_NAMED)
#define FILTER_WITHSIZE (FILTER_PASS_WITHSIZE | FILTER_REMOVE_WITHSIZE)
#define FILTER_PRIVATE  (FILTER_PASS_PRIVATE  | FILTER_REMOVE_PRIVATE)

int islocation(char l) {
	return ((l >= 'g') && (l <= 'z')) || ((l >= 'G') && (l <= 'Z'));
}

int sprint_entry(char *c,const char *hash,int dir,int base) {
	struct fn_ent_s *fn;
	long p;
	int n = 0,m;
	char index = '0';
	char l[3];
	int flen;

	if (hash[0] != '.') {
		p = find_fn(hash,'0');
		if (p == -1)
		        n = sprintf(c,"(unknown hash %.8s)",hash);
		else {
			fn = &fn_addr[p];
			if (dir) {
				n = sprint_entry(c,fn->dirhash,1,1);
				if (base == 0)
					return n;
				c += n;
				if (n > 0) {
					*c++ = '/';
			       		n++;
				}
			}
			
			l[0] = fn->namelen[0];
			l[1] = fn->namelen[1];
			l[2] = 0;
			sscanf(l,"%x",&flen);
			//sprintf(c,"(%s)",l); n += 4;	c += 4;
			while (flen > 0) {
				if (flen >= 32) {
					m = sprintf(c,"%.32s",fn->name);
					c += m;
					n += m;
					flen -= 32;
					if (flen > 0) {
						index++;
						p = find_fn(hash,index);
						if (p == -1)
						c += sprintf(c,"(unknown hash)");
						else
						fn = &fn_addr[p];
					}
				}
				else {
					int i = 0;
					while (flen-- > 0) {
					      *c++ = fn->name[i++];
					      n++;
				        }
				}
			}
		}
	}
	return n;
}

void output_line(const char *line,const struct md5_ent_s *e) {
	char buf[10000],*c,*f,ch;
	long long fsize;
	
	c = &buf[0];
	f = cmdopt.format;
	while((ch = *f++) != 0) {
		if (ch == '\\') {
			ch = *f++;
			if (ch == 0)
				break;
			if (ch == 'n')
				*c++ = '\n';
	        }
		else if (ch == '%') {
			ch = *f++;
			if (ch == 0)
				break;
			if (ch == '%')
				*c++ = ch;
			if (ch == 'l') {
			    	strcpy(c,line);
				c += strlen(line);
			}
			if (ch == 'm') {
			    	strncpy(c,e->md5,32);
				c += 32;
			}
			if (ch == 'M') {
				*c++ = '/';
			    	strncpy(c,e->md5,2); c += 2;
				*c++ = '/';
			    	strncpy(c,&e->md5[2],2); c+= 2;
				*c++ = '/';
			    	strncpy(c,e->md5,32);
				c += 32;
			}
			if (ch == 'N') {
				*c++ = '/';
			    	strncpy(c,e->md5,2); c += 2;
				*c++ = '/';
			    	strncpy(c,&e->md5[2],2); c+= 2;
				*c++ = '/';
			    	strncpy(c,&e->md5[4],2); c+= 2;
				*c++ = '/';
			    	strncpy(c,&e->md5[6],26);
				c += 26;
			}
			if (ch == 's') {
				if (e->filelen[0] == '.')
					*c++ = '?';
				else {
					strncpy(c,e->filelen,10);
					c[10] = 0;
					sscanf(c,"%llX",&fsize);
					c += sprintf(c,"%lld",fsize);
				}
			}
			if ((ch == 'f') || (ch == 'd') || (ch == 'p')) {
				int n = 0;

				if ((ch == 'd') || (ch == 'p'))
					c += n = sprint_entry(c,e->namehash,1,0);
				if ((n > 0) && (ch == 'p')) {
					*c++ = '/';
					n++;
				}
				if ((ch == 'f') || (ch == 'p'))
					c += sprint_entry(c,e->namehash,0,1);
			}
			if (ch == 'b') {
			   	int n;
				
			   	n = sizeof(struct md5_ent_s)-1;
			   	strncpy(c,(char *)e,n);
				c += n;
			}
			if (ch == 'L') {
			   	strncpy(c,e->location,NR_LOCATION);
				c += NR_LOCATION;
			}
		}
		else
			*c++ = ch;
	}
	*c = 0;
	fputs(buf,stdout);
}

void filter_file(FILE *in,int filter) {
	char line[MAX_LINESIZE];
	int ok,pass,remove;
	int i,n;
	long k;
	const struct md5_ent_s *e;
	
	fgets(line,MAX_LINESIZE,in);
	while (!feof(in)) {
		n = strlen(line);
		if (line[n-1] == '\n')
			line[--n] = 0;
		ok = (n >= 32);
		if (ok) {
			for (i = 0;i <= 31;i++) 
				if (!isxdigit(line[i]))
					ok = 0;
//			if (isxdigit(line[32]))
//				ok = 0;
		}
		if (ok) {
			pass = 0;
			remove = 0;

			k = find_md5(line);
			if ((filter & FILTER_KNOWN) == FILTER_PASS_UNKNOWN) {
				pass   |= (k < 0);
				remove |= (k >= 0);
			}
			e = &md5_empty;
			if (k >= 0) {
				e = &md5_addr[k];
				if ((filter & FILTER_KNOWN    ) == FILTER_PASS_KNOWN)
					pass   |= 1;
				if ((filter & FILTER_IGNORE    ) == FILTER_PASS_IGNORE)
					pass   |= (e->qual == '?');
				if ((filter & FILTER_IGNORE  ) == FILTER_REMOVE_IGNORE)
					remove |= (e->qual == '?');
				if ((filter & FILTER_NAMED     ) == FILTER_PASS_NAMED)
					pass   |= (e->namehash[0] != '.');
				if ((filter & FILTER_NAMED   ) == FILTER_REMOVE_NAMED)
					remove |= (e->namehash[0] != '.');
				if ((filter & FILTER_PRIVATE   ) == FILTER_PASS_PRIVATE)
					pass   |= (e->qual == '&');
				if ((filter & FILTER_PRIVATE ) == FILTER_REMOVE_PRIVATE)
					remove |= (e->qual == '&');
				if ((filter & FILTER_WITHSIZE  ) == FILTER_PASS_WITHSIZE)
					pass   |= (e->filelen[0] != '.');
				if ((filter & FILTER_WITHSIZE) == FILTER_REMOVE_WITHSIZE)
					remove |= (e->filelen[0] != '.');
				if ((filter & FILTER_LOCATION) == FILTER_LOCATION) {
					int m = 0;
					n = 0;
					for (i = 0;i < NR_LOCATION;i++) {
						int l;
						l = (int)e->location[i];
						if (l != '_')
							m++;
						if (cmdopt.filter_location[l] == 'P')
						        n++;
						remove |= (cmdopt.filter_location[l] == 'R');
					}
					pass |= (n == cmdopt.filter_loc_count);

					pass   |= (cmdopt.filter_location['0' + m] == 'P');
					remove |= (cmdopt.filter_location['0' + m] == 'R');
				}
			}
			if (pass && !remove)
			        output_line(line,e);
		}
		fgets(line,MAX_LINESIZE,in);
	}
}

void usage(const char *cmd) {
	FILE *chan=stdout;
	fprintf(chan,"Usage: %s [<options>]\n\n",cmd);
	fprintf(chan,"   Database Management:\n");
	fprintf(chan,"       -B <database> ... md5 database name (default: %s)\n",md5_database_default);
	fprintf(chan,"       -H <file>     ... replace header of md5 database\n\n");
	fprintf(chan,"       -N <database> ... name database name (default: %s)\n",fn_database_default);
	fprintf(chan,"       -X <file>     ... replace header of name database\n\n");
	fprintf(chan,"       -c[k]         ... calculate statistics for file lengths (-ck in 1k blocks.\n");
	fprintf(chan,"       -v            ... verbose\n");
	fprintf(chan,"   Input file:\n");
	fprintf(chan,"       -M <file>     ... read md5s to process from <file>. See note below\n\n");
	fprintf(chan,"   Modify database:\n");
	fprintf(chan,"       -w            ... enable write to database\n");
	fprintf(chan,"       -I            ... mark entry as ignore (with -M).\n");
	fprintf(chan,"       -p            ... mark entry as private.\n");
	fprintf(chan,"       -b            ... mark entry as public.\n");
	fprintf(chan,"       -S            ... add filesizes which should be after md5.\n");
	fprintf(chan,"       -a <char>     ... add location code for md5s\n");
	fprintf(chan,"       -d <char>     ... delete location code for md5s\n");
	fprintf(chan,"       -P <dir>      ... Add filename with path in database under <dir>.\n\n");
	fprintf(chan,"   Filter/Pipe mode:\n");
//	fprintf(chan,"       -f <code>     ... filter md5s supplied with -M\n");
//	fprintf(chan,"          k-<ipns>           leading k : all known are passed through. \n");
//	fprintf(chan,"                                         following qualifiers are removed.\n");
//	fprintf(chan,"          u+<ipns>           leading u : unknown are passed through. \n");
//	fprintf(chan,"                                         following qualifiers are removed.\n");
//	fprintf(chan,"                         -k-ipns       ... pass known md5s\n");
	fprintf(chan,"       -u            ... filter md5s and output only unknown (not in database)\n");
	fprintf(chan,"       -k            ... pass known md5s\n");
	fprintf(chan,"       -i+ | -i-     ... pass/filter known ignored md5s\n");
	fprintf(chan,"       -p+ | -p-     ... pass/filter known private md5s\n");
	fprintf(chan,"       -n+ | -n-     ... pass/filter known         md5s with name\n");
	fprintf(chan,"       -s+ | -s-     ... pass/filter known         md5s with size\n");
	fprintf(chan,"       -l+<l>*<n>*   ... pass        known         md5s stored in ALL location <l>,..\n");
	fprintf(chan,"                                                        or in <n> locations\n");
	fprintf(chan,"       -l-<l>*<n>*   ... filter      known         md5s stored in ANY location <l>,..\n");
	fprintf(chan,"                                                        or in <n> locations\n");
	fprintf(chan,"       -o <format>   ... instead of copying the lines from input file,\n");
	fprintf(chan,"                         output the text based on the format string.\n");
	fprintf(chan,"                            %%l = input line\n");
	fprintf(chan,"                            %%m = md5\n");
	fprintf(chan,"                            %%M = md5 in form /xx/yy/xxyy.....\n");
	fprintf(chan,"                            %%N = md5 in form /xx/yy/zz/aa....\n");
	fprintf(chan,"                            %%f = filename\n");
	fprintf(chan,"                            %%d = directory\n");
	fprintf(chan,"                            %%p = filename with directory\n");
	fprintf(chan,"                            %%s = filesize\n");
	fprintf(chan,"                            %%b = database entry\n");
	fprintf(chan,"                            %%L = locations from database\n");
	fprintf(chan,"\n");
	fprintf(chan,"       Note:  if <file> is '-', then read stdin\n");
	fprintf(chan,"              if <file> is 'db', then read the database (implies filtering)\n");
	fprintf(chan,"              if -S and -P are both given, then the file should be:\n");
	fprintf(chan,"                   ^<32*char md5> *<filelen> *<path>$\n");
	fprintf(chan,"\n");
	exit(EXIT_FAILURE);
}

void pmkdir(char *dir) {
	int i,rc;
	
	if (access(dir,F_OK) == 0)
		return;
	rc = mkdir(dir,0777);
	if (rc) {
		i = strlen(dir);
		while (i-- > 0) {
			if (dir[i] == '/') {
				dir[i] = 0;
				pmkdir(dir);
				dir[i] = '/';
			}
		}
		rc = mkdir(dir,0777);
	}
	if (rc == 0)
	        dbstat.dirsadded++;
}

u32 fn_add_entry(struct md5_ent_s *md5,char *fpath) {
	u32 hash;
	u32 dirhash;
	char h[9];
	char l[3];
	int pos,flen,plen;
	struct fn_ent_s enew;
	long p;

	plen = strlen(fpath);

	// Find split point between path and filename.
	for (pos = plen-1;pos >= 0;pos--)
		if (fpath[pos] == '/')
			break;

	if (pos < 0) // no directory in path
	        flen = plen;
	else
	        flen = plen - pos - 1;
	sprintf(l,"%.02X",flen);

	// calculate hash for the full pathname including filename/dirname
	hash = calc_namehash(fpath);
	do {
		// check for collision
		sprintf(h,"%.08lX",(unsigned long)hash);
		p = find_fn(h,'0');
		if (p >= 0) {
		        // Found an entry with this hash, so check for collision:
			//       Compare length and first 32 bytes
			if ((strncmp(l,fn_addr[p].namelen,2) == 0)
			    && (strncmp(&fpath[pos+1],fn_addr[p].name,flen>32?32:flen-1) == 0)) {
				if (cmdopt.verbose) 
					fprintf(stderr,"Found matching hash %.08lX\n",(unsigned long)hash);
				return hash; // entry exists, so return the hash
			}
			if (cmdopt.verbose) {
				fprintf(stderr,"Found matching hash %.08lX, but not same\n",(unsigned long)hash);
				fprintf(stderr,"...%.32s / %.2s\n",fn_addr[p].name,fn_addr[p].namelen);
				fprintf(stderr,"...%.32s / %.2s\n",&fpath[pos+1],l);
			}
			// check next hash value after collision    
			if (hash & 1)
				hash = (hash>>1) ^ 0xdeadbeaf;
			else
				hash >>= 1;
		}
	} while (p >= 0);
	
	// New entry to be added with the hash as key.
	enew = fn_empty;
	strncpy(enew.hash,h,8);
	enew.index = '0';
	enew.namelen[0] = l[0];
	enew.namelen[1] = l[1];
	
	// If fpath contains a directory, then calculate the dirhash
	if ((fpath[pos] == '/') && (pos > 0)) {
		// get the dirhash
		fpath[pos] = 0;
		dirhash = fn_add_entry(NULL,fpath);
		fpath[pos] = '/';
		sprintf(h,"%.08lX",(unsigned long)dirhash);
		strncpy(enew.dirhash,h,8);
	}

	// Here the new entry is fully prepared, only name to be filled in.

	// fill in the hash in the md5 table
	if (md5) 
	        strncpy(md5->namehash,enew.hash,8);

	pos++;
	while (pos < plen) {
		if (plen-pos > 32) {
			strncpy(enew.name,&fpath[pos],32);
			pos += 32;
		}
		else {
			strncpy(enew.name,fn_empty.name,32);
			strncpy(enew.name,&fpath[pos],plen-pos);
			pos = plen; 
		}
		// add entry
		do {
			p = find_free_fn_entry(enew.hash);
			if (p == -1)
				grow_fn(fn_fd,2*fn_addr_n);
		} while(p < 0);

		fn_addr[p] = enew;
		
		// prepare next index
		enew.index++;
	}

	// printf("add_entry...%s<\n...%s<\n...%.8lX\n",fpath,&fpath[pos],hash);
	return hash;
}

void process_md5(FILE *in) {
	char line[MAX_LINESIZE];
	char *rd;
	int ok;
	int pos;
	int i;
	struct md5_ent_s *e;
	
	rd = fgets(line,MAX_LINESIZE,in);
	while (rd && !feof(in)) {
		if (cmdopt.verbose)
		        printf("%s\n",line);
		ok = 1;
		for (i = 0;i <= 31;i++) {
			if (!isxdigit(line[i])) {
				ok = 0;
				break;
			}
		}
		if (ok) {
			{
				long p;
				p = find_md5(line);
				if (p == -1) {
				        dbstat.added++;
					p = find_free_md5_entry(line);
					while (p == -1) {
						grow_md5(md5_fd,2*md5_addr_n);
						p = find_free_md5_entry(line);
					}
					assert(md5_addr[p].md5[0] == '#');
				}
				else {
					assert(strncmp(line,md5_addr[p].md5,32)==0);
					dbstat.known++;
				}
				e = &md5_addr[p];
			}
			strncpy(e->md5,line,32);
			if (cmdopt.add_code != ' ') {
				for (i = 0;i < NR_LOCATION;i++) {
					if (e->location[i] == cmdopt.add_code)
						break;
					if (e->location[i] == '_') {
						e->location[i] = cmdopt.add_code;
						break;
					}
				}
				if ((i == NR_LOCATION) && (e->qual != '?'))
				        fprintf(stderr,"no location field available for %.32s\n",e->md5);
			}
			if (cmdopt.del_code != ' ') {
				for (i = 0;i < NR_LOCATION-1;i++) {
					if (e->location[i] == cmdopt.del_code) {
						e->location[i] = e->location[i+1];
						e->location[i+1] = cmdopt.del_code;
					}
				}
				if (e->location[NR_LOCATION-1] == cmdopt.del_code)
				        e->location[NR_LOCATION-1] = '_';
			}
			if (cmdopt.ignore)
				e->qual = '?';
			pos = 33;
			if (cmdopt.readsize) {
				long long size;
				char slen[11];
				
				while (line[pos] == ' ') pos++;
				if ((line[pos] >= '0') && (line[pos] <= '9')) {
					size = 0;
					while ((line[pos] >= '0') && (line[pos] <= '9')) {
						size *= 10;
						size += (line[pos] - '0');
						pos++;
					}
					sprintf(slen,"%-.10llX",size);
					if (e->filelen[0] != '.') {
						if (strncmp(e->filelen,slen,10) != 0) {
							fprintf(stderr,"filelen mismatch: %.32s: %.10s != %s\n",
							    e->md5,e->filelen,slen);
							exit(1);
						}
					}
					else {
						strncpy(e->filelen,slen,10);
						dbstat.lengthset++;
					}
				}
			}
			if (cmdopt.dirprefix) {
				if (e->qual == '.') { // do not add twice to git
					char dirfname[MAX_LINESIZE+500];
//					char fname[500];
//					FILE *dirlist;
					int baselen;

					while (line[pos] == ' ') pos++;
					if (line[pos] == 0)
						continue;
					
					// remove trailing new line and all spaces
					do {
					   line[strlen(line)-1] = 0; // replace \n with 0
					} while (line[strlen(line)-1] == ' ');

					baselen = strlen(cmdopt.dirprefix);
					strcpy(dirfname,cmdopt.dirprefix);
					strcpy(&dirfname[baselen],&line[pos]);

					// In dirfname is now the concatenation
					//		basepath/ subdir/filename
					// dirfname[baselen-1] is '/'
					pos = strlen(dirfname);
					while (dirfname[--pos] != '/')
					        assert(pos >= baselen);

					fn_add_entry(e,&dirfname[baselen]);
					e->qual = '@';
				}
			}
			if (cmdopt.private) {
			   if (e->qual == '@')
			   	e->qual = '&';
			}
			if (cmdopt.public) {
			   if (e->qual == '&')
			   	e->qual = '@';
			}
		}
		rd = fgets(line,MAX_LINESIZE,in);
	}
	
	printf("MD5: known=%ld  added=%ld    FILE: added=%ld   size set=%ld   DIR: added=%ld\n",
	    dbstat.known,dbstat.added,dbstat.filesadded,dbstat.lengthset,dbstat.dirsadded);
	
}

char *pretty(long long t,char *buf,int bufsize,int minlen) {
	int i,j,m;

	sprintf(buf,"%lld",t);
	i = strlen(buf);
	j = bufsize-1;
	buf[j--] = 0;
	m = 0;
	while (i > 0) {
		minlen--;
		buf[j--] = buf[--i];
		if ((++m == 3) && (i > 0)) {
			minlen--;
			buf[j--] = '.';
			m = 0;
		}
	}
	while(minlen-- > 0)
		buf[j--] = ' ';
	return &buf[j+1];
}

void calculate_size() {
	long p;
	long long fsize;
	int i;
	struct md5_ent_s *e;
	char l[40];
	char loc;
	int numlen;

	for (i = 0;i < 256;i++) {
		dbstat.sum_location[i] = 0;
		dbstat.sum_obs_location[i] = 0;
		dbstat.sum_unq_location[i] = 0;
		dbstat.sum_len_location[i] = 0;
		dbstat.sum_unn_location[i] = 0;
	}
	
	e = &md5_addr[0];
	for (p = 0;p < md5_addr_n;p++,e++) {
		if (e->md5[0] != '#') {
		        if (e->filelen[0] != '.') {
				strncpy(l,e->filelen,10);
				l[10]=0;
				sscanf(l,"%llX",&fsize);
				if (cmdopt.len_1kblks)
					fsize = (fsize+1023)/1024;
			}
			else {
				fsize = 0;
				if (e->qual != '?')
				        dbstat.files_without_length++;
			}

			for (i = 0;i < 7;i++) {
				loc = e->location[i];
				if (loc != '_') {
					dbstat.sum_location[(int)loc] += fsize;
					if (e->qual == '.')
					        dbstat.sum_unn_location[(int)loc]++;
					if (e->qual == '?')
					        dbstat.sum_obs_location[(int)loc] += fsize;
					else {
						if ((i == 0) && (e->location[1] == '_'))
						       dbstat.sum_unq_location[(int)loc] += fsize;
						if (fsize == 0)
					               dbstat.sum_len_location[(int)loc]++;
					}
				}
			}
			dbstat.sum_all_entries += fsize;
			if (e->qual == '?')
			       dbstat.sum_all_ignored += fsize;
			else {
			       dbstat.sum_all_ok      += fsize;
			       if (e->location[0] == '_')
			                dbstat.sum_all_without_location += fsize;
		       }
		}
	}

	if (cmdopt.len_1kblks) {
		numlen = 13;
		printf("\nAll sizes in blocks of 1 kByte.\n");
	}
	else {
		numlen = 17;
		printf("\nAll sizes in Byte.\n");
	}
	
	printf("total size of all entries = %s\n",pretty(dbstat.sum_all_entries,l,40,numlen));
	printf("              all ignored = %s\n",pretty(dbstat.sum_all_ignored,l,40,numlen));
	printf("              all ok      = %s\n",pretty(dbstat.sum_all_ok,l,40,numlen));
	printf("              without loc = %s\n",pretty(dbstat.sum_all_without_location,l,40,numlen));
	printf("\n");
	printf("                                                                 unknown\n");
	printf("Loc         size             obsolete            unique      length   name\n");
	{
		long long sum_size=0,sum_obs=0;
		for (i = 0;i < 256;i++) {
			if (dbstat.sum_location[i] > 0) {
				sum_size += dbstat.sum_location[i];
				sum_obs  += dbstat.sum_obs_location[i];
				
				printf(" %c: %s",i,pretty(dbstat.sum_location[i],l,40,numlen));
				printf(" %s",pretty(dbstat.sum_obs_location[i],l,40,numlen));
				printf(" %s %7ld %7ld  %s\n",pretty(dbstat.sum_unq_location[i],l,40,numlen),
				    dbstat.sum_len_location[i],dbstat.sum_unn_location[i],
				    &location[i][0]);
			}
		}
		printf("    -----------------------------------\n");
		printf("    %s",pretty(sum_size,l,40,numlen));
		printf(" %s\n",pretty(sum_obs,l,40,numlen));
	}
	printf("\n");
	printf("# of files without length: %s\n",pretty(dbstat.files_without_length,l,40,10));
}

void read_locations_from_header() {
	int i,n,table,c,c1,c2;
	char hdr[HEADERLEN+2];
	
	for (i = 0;i < 256;i++)
		location[i][0] = 0;
	
	strncpy(hdr,md5_header,HEADERLEN);
	for (i = 0;i < HEADERLEN;i++)
		if (hdr[i] == '\n')
			hdr[i] = 0;

	n = 0;
	while (n < 4096) {
		if (table == 1) {
			c1 = c2 = 0;
			c = 0;
			while (hdr[n+c]) {
				if ((hdr[n+c] == ' ') && (hdr[n+c+2] == ' ') 
				        && ((hdr[n+c+1] == 'g') || (hdr[n+c+1] == 'G')) ) {
					c1 = c2; c2 = c + 1;
				}
				c++;
			}
			if (c1 != 0)
				table = 2;
			else
				table = 0;
		}
		if (table == 2) {
			if ((hdr[n+c1] ^ hdr[n+c2]) != 32)
				table = 0;
			else {
				strncpy(&location[(int)hdr[n+c1]][0],&hdr[n+c1+2],c2-c1-3);
				strncpy(&location[(int)hdr[n+c2]][0],&hdr[n+c2+2],strlen(&hdr[n])-c2-3);
			}
		}
		if (strncmp(&hdr[n],"# Defined locations:",20) == 0)
			table = 1;
		n += strlen(&hdr[n])+1;
	}

	for (i = 0;i < 256;i++) {
		for (c = strlen(&location[i][0]);c >= 0;c++) {
			if (location[i][c] != ' ')
				break;
			location[i][c] = 0;
		}
	}
}

int main(int argc,char **argv) {
	int tmp_fd;
	FILE *in=NULL;
	char md5_head[HEADERLEN];
	char fn_head[HEADERLEN];
	int opt;
	int md5_hdr = 0;
	int fn_hdr = 0;
	int i;

	assert(sizeof(u8) == 1);
	assert(sizeof(u32) == 4);
	
	{
		int i;
		for (i = 0;i < HEADERLEN/64;i++) {
			strncpy(&md5_head[i*64   ],"#...............................",32);
			strncpy(&md5_head[i*64+32],"...............................\n",32);
			strncpy(&fn_head[i*64   ],"#...............................",32);
			strncpy(&fn_head[i*64+32],"...............................\n",32);
		}
	}

	cmdopt.md5_database = md5_database_default;
	cmdopt.fn_database = fn_database_default;

	if (argc == 1) {
		printf("MDDB=%s\n",cmdopt.md5_database);
		printf("FNDB=%s\n",cmdopt.fn_database);
		exit(0);
	}
	
	while ((opt = getopt(argc, argv, "wbH:M:l:d:IB:N:X:P:vSc::ui:n:s:a:p::ko:")) != -1) {
		switch (opt) {
		case 'H':
			tmp_fd = open(optarg,O_RDONLY);
			if (tmp_fd == -1) {
				fprintf(stderr,"File not found: %s\n",optarg);
				exit(100);
			}
			md5_hdr = read(tmp_fd,md5_head,HEADERLEN-1);
			close(tmp_fd);
			if (md5_hdr > 0)
			        md5_head[md5_hdr] = '\n';
			break;
		case 'X':
			tmp_fd = open(optarg,O_RDONLY);
			if (tmp_fd == -1) {
				fprintf(stderr,"File not found: %s\n",optarg);
				exit(100);
			}
			fn_hdr = read(tmp_fd,fn_head,HEADERLEN-1);
			close(tmp_fd);
			if (fn_hdr > 0)
			        fn_head[fn_hdr] = '\n';
			break;
		case 'M':
			if (*optarg == '-')
				in = stdin;
			else if (strcmp(optarg,"db") == 0) {
				in = fopen(cmdopt.md5_database,"r");
				if (in == NULL) {
					fprintf(stderr,"File not found: %s\n",cmdopt.md5_database);
					exit(100);
				}
				cmdopt.filter_type = FILTER_ENABLE;
			}
			else {
				in = fopen(optarg,"r");
				if (in == NULL) {
					fprintf(stderr,"File not found: %s\n",optarg);
					exit(100);
				}
			}
			break;
		case 'o':
			cmdopt.format = optarg;
			break;
		case 'B':
			cmdopt.md5_database = optarg;
			break;
		case 'N':
			cmdopt.fn_database = optarg;
			break;
		case 'S':
			cmdopt.readsize = 1;
			break;
		case 'w':
			cmdopt.enable_write = 1;
			break;
		case 'b':
			cmdopt.public = 1;
			break;
		case 'p':
			if (optarg) {
				if (strlen(optarg) != 1)
					usage(argv[0]);
				if (optarg[0] == '+')
				        cmdopt.filter_type |= FILTER_PASS_PRIVATE;
				else if (optarg[0] == '-')
			                cmdopt.filter_type |= FILTER_REMOVE_PRIVATE;
			        else
 				        usage(argv[0]);
			}
			else
			        cmdopt.private = 1;
			break;
		case 'P':
			if (optarg[strlen(optarg)-1] != '/') {
				fprintf(stderr,"directory <%s> does not end with /\n",optarg);
				exit(100);
			}
			cmdopt.dirprefix = optarg;
			break;
		case 'a':
			if (strlen(optarg) != 1)
				usage(argv[0]);
			if (!islocation(optarg[0]))
				usage(argv[0]);
			cmdopt.add_code = *optarg;
			break;
		case 'd':
			if (strlen(optarg) != 1)
				usage(argv[0]);
			if (!islocation(optarg[0]))
				usage(argv[0]);
			cmdopt.del_code = *optarg;
			break;
		case 'I':
			cmdopt.ignore = 1;
			break;
		case 'v':
			cmdopt.verbose = 1;
			break;
		case 'u':
			cmdopt.filter_type |= FILTER_PASS_UNKNOWN;
			break;
		case 'k':
			cmdopt.filter_type |= FILTER_PASS_KNOWN;
			break;
		case 'i':
			if (strlen(optarg) != 1)
				usage(argv[0]);
			if (optarg[0] == '+')
			      cmdopt.filter_type |= FILTER_PASS_IGNORE;
			else if (optarg[0] == '-')
			      cmdopt.filter_type |= FILTER_REMOVE_IGNORE;
			else
				usage(argv[0]);
			break;
		case 'n':
			if (strlen(optarg) != 1)
				usage(argv[0]);
			if (optarg[0] == '+')
			      cmdopt.filter_type |= FILTER_PASS_NAMED;
			else if (optarg[0] == '-')
			      cmdopt.filter_type |= FILTER_REMOVE_NAMED;
			else
				usage(argv[0]);
			break;
		case 's':
			if (strlen(optarg) != 1)
				usage(argv[0]);
			if (optarg[0] == '+')
			      cmdopt.filter_type |= FILTER_PASS_WITHSIZE;
			else if (optarg[0] == '-')
			      cmdopt.filter_type |= FILTER_REMOVE_WITHSIZE;
			else
				usage(argv[0]);
			break;
		case 'l':
			{
				char fil;
				cmdopt.filter_type |= FILTER_LOCATION;
				if (strlen(optarg) < 2)
				        usage(argv[0]);
				if (optarg[0] == '+')
				        fil = 'P';
				else if (optarg[0] == '-')
				        fil = 'R';
				else
				        usage(argv[0]);
				for (i = 1;i < strlen(optarg);i++) {
					char x = optarg[i];
					
				        if (islocation(x)) {
						if ((cmdopt.filter_location[(int)x] != 'P')
						                    && (fil == 'P')) {
						       if (cmdopt.filter_loc_count < 0)
						               cmdopt.filter_loc_count = 0;
						       cmdopt.filter_loc_count++;
					        }
					}
					else if (!((x >= '0') && (x <= '8')))
						usage(argv[0]);
					cmdopt.filter_location[(int)x] = fil;
				}
			}
			break;
		case 'c':
			if (optarg) {
				if (strlen(optarg) != 1)
				        usage(argv[0]);
				if (optarg[0] != 'k')
				        usage(argv[0]);
				cmdopt.len_1kblks = 1;
			}
			cmdopt.calc_stats = 1;
			break;
		default: // '?'
			usage(argv[0]);
		}
	}
//	if (optind >= argc) {
//		fprintf(stderr, "Expected argument after options\n");
//		exit(EXIT_FAILURE);
//	}

	if ((cmdopt.filter_type & 3) == 3) {
		fprintf(stderr, "Error in filter options: either single -u or one/several of -i/x/s+/-\n");
		exit(EXIT_FAILURE);
	}

	md5_fd = open(cmdopt.md5_database,cmdopt.enable_write ? O_RDWR:O_RDONLY);
	if (md5_fd == -1) {
		fprintf(stderr,"Cannot open database file\n");
		exit(100);
	}
	prepare_md5(md5_fd);		
	if (md5_addr_n == 0) {
		grow_md5(md5_fd,2);
		md5_hdr = HEADERLEN;
	}
	if (md5_hdr > 0)
	        strncpy(md5_header,md5_head,HEADERLEN);

	fn_fd = open(cmdopt.fn_database,cmdopt.enable_write ? O_RDWR:O_RDONLY);
	if (fn_fd == -1) {
		fprintf(stderr,"Cannot open database file\n");
		exit(100);
	}
	prepare_fn(fn_fd);		
	if (fn_addr_n == 0) {
		grow_fn(fn_fd,2);
		fn_hdr = HEADERLEN;
	}
	if (fn_hdr > 0)
	        strncpy(fn_header,fn_head,HEADERLEN);

	read_locations_from_header();
		
	if (cmdopt.filter_type && in) {
		filter_file(in,cmdopt.filter_type);
		fclose(in);
		in = NULL;
	}

	if (in) {
		process_md5(in);
		fclose(in);
		in = NULL;
	}

	if (cmdopt.calc_stats) {
		calculate_size();
	}
	
	if (md5_addr)
		munmap(md5_addr,md5_addr_n * sizeof(struct md5_ent_s));
	if (md5_header)
		munmap(md5_header,HEADERLEN);
	close(md5_fd);

	if (fn_addr)
		munmap(fn_addr,fn_addr_n * sizeof(struct fn_ent_s));
	if (fn_header)
		munmap(fn_header,HEADERLEN);
	close(fn_fd);
	
	return 0;
}
