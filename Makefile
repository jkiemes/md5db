CFLAGS=-Wall -g

md5db:	md5db.c

stat:
	@gawk '/^[0-9a-f]/{s=0;next} {s++;if(s>maxs) maxs=s;} END{print "max sequence of #:"maxs}' md5.db
	@gawk '/^#/{s=0;next} {s++;if(s>maxs) maxs=s;} END{print "max sequence of md5:" maxs}' md5.db

clean:
	find . -name "*~" -exec rm "{}" \;
